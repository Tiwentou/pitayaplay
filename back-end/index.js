export default {
    gameItems: [
        {
            id: "tropico_5",
            title:"Un jeu critique",
            name: "tropico_5",
            description: "jeu de gestion sur une île des caraïbes rappelant la situation de Cuba au sortant de la 2nd WW",
            developer:"Haemimont Games",
            picUrl:"https://vignette.wikia.nocookie.net/tropico/images/c/cf/T5_USA_PC_2D.jpg/revision/latest?cb=20140428163525",
            plateform: "PC",
            category: "gestion",
            notation: "7",
        },
        {
            id: "gta_5",
            title:"Le jeu le plus violent !",
            name: "gta_5",
            description: "jeu défouloire reprenant comme toujours les mêmes ingrédients de la série mythique",
            developer:"Rockstar Games",
            picUrl:"https://gamespot1.cbsistatic.com/uploads/scale_landscape/mig/6/8/4/4/2286844-gtalogo-big_61199_screen.jpg",
            plateform: "PS4",
            category: "action",
            notation: "8",
        },
        {
            id: "okami",
            title:"Un jeu coup de coeur",
            name: "okami",
            description: "jeu d'aventure au graphisme époustouflants",
            developer:"Capcom",
            picUrl:"https://pngimage.net/wp-content/uploads/2018/06/okami-logo-png-5.png",
            plateform: "Wii",
            category: "aventure",
            notation: "9",
        },
        {
            id: "jump_force",
            title:"Une purge",
            name: "jump_force",
            description: "jeu de combat se reposant sur le fan-service",
            developer:"Spike Chunsoft",
            picUrl:"https://media.playstation.com/is/image/SCEA/jump-force-logo-01-ps4-us-05nov18?$native_t$",
            plateform: "PS3",
            category: "fight",
            notation: "3",
        },
    ],
    gamesCts: [
        {
            id: "tropico_5cts_1",
            nom: "Xx-DarkSasuke-xX",
            mail: "Deums@caramail.com",
            text: "bon"
        },
        {
            id: "jump_forcects_1",
            nom: "Xx-OuiOui-xX",
            mail: "Ouioui@caramail.com",
            text: "nul !!!"
        },
        {
            id: "jump_forcects_2",
            nom: "Xx-NonNon-xX",
            mail: "Nonnon@caramail.com",
            text: "oui !!!"
        },
    ]
};